package guy.droid.im.pdfcreate;


import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.hendrix.pdfmyxml.PdfDocument;
import com.hendrix.pdfmyxml.viewRenderer.AbstractViewRenderer;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    AbstractViewRenderer page;
    PdfDocument doc     ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        doc            = new PdfDocument(MainActivity.this);
        pdfcreates();
    }

    public void pdfcreates()
    {
        page = new AbstractViewRenderer(getApplicationContext(), R.layout.pdf_sample) {
            private String _text;

            public void setText(String text) {
                _text = text;
            }

            @Override
            protected void initView(View view) {
                TextView tv_hello = (TextView)view.findViewById(R.id.tv_hello);
                ImageView ima = (ImageView) view.findViewById(R.id.img);
                ima.setImageResource(R.mipmap.ic_launcher);
                ima.setBackgroundColor(Color.BLUE);
                tv_hello.setText("TEST");
            }
        };
        runOnUiThread(new Runnable() {
            public void run() {
                doc.addPage(page);

                doc.setRenderWidth(2115);
                doc.setRenderHeight(1500);
                doc.setOrientation(PdfDocument.A4_MODE.LANDSCAPE);
                // doc.setProgressTitle("CREATING".toString());
                //  doc.setProgressMessage("");
                doc.setFileName("testraz");
                doc.setInflateOnMainThread(false);
                doc.setListener(new PdfDocument.Callback() {
                    @Override
                    public void onComplete(File file) {
                        Toast.makeText(MainActivity.this, "Com"+file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                        Log.i(PdfDocument.TAG_PDF_MY_XML, "Complete");
                    }

                    @Override
                    public void onError(Exception e) {
                        Toast.makeText(MainActivity.this, "Err", Toast.LENGTH_SHORT).show();
                        Log.i(PdfDocument.TAG_PDF_MY_XML, "Error");
                    }
                });

                doc.createPdf(MainActivity.this);
            }
        });



    }


}
